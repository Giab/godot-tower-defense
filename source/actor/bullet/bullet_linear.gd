extends Bullet
class_name BulletLinear

## 线性非跟踪子弹
const EXPLOTION = preload("res://source/actor/explotion.tscn")

var _target_position
var direction : Vector2
var is_explote : bool = false

func _ready() -> void:
	#await get_tree().create_timer(5).timeout
	#queue_free()
	var tween : Tween = create_tween()
	tween.tween_callback(queue_free).set_delay(1)
	super()

func _process(delta: float) -> void:
	if is_explote : return
	var step = speed * delta
	#var direction = (_target_position - global_position).normalized()
	position += direction * step
	#if global_position.distance_to(_target_position) <= step:
		#queue_free()

func initialize(target : Enemy) -> void:
	super(target)
	_target_position = target.global_position
	direction = (_target_position - global_position).normalized()
	rotation = direction.angle()

## 产生爆炸物
func spawn_explotion() -> Explotion:
	var explotion : Explotion = EXPLOTION.instantiate()
	explotion.damage = damage * 0.8
	add_child(explotion)
	is_explote = true
	return explotion

func attack(enemy : Enemy) -> void:
	var explotion = spawn_explotion()
	await explotion.exploted
	if enemy:
		super(enemy)
